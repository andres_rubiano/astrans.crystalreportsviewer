﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ReportViewer.aspx.cs" Inherits="Simplexity.CrystalReportViewer.ReportViewer" %>

<%@ Register assembly="CrystalDecisions.Web, Version=13.0.3500.0, Culture=neutral, PublicKeyToken=692fbea5521e1304" namespace="CrystalDecisions.Web" tagprefix="CR" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
<script src='<%=ResolveUrl("~/crystalreportviewers13/js/crviewer/crv.js")%>' type="text/javascript"></script>
</head>
<body>
    <form id="frm_ReportViewer" runat="server">
        <div style="font-family: Arial, Helvetica, sans-serif; font-size: large; font-weight: bold; color: #FF3300">
            <CR:CrystalReportSource ID="CrystalReportSourceOne" runat="server">
            </CR:CrystalReportSource>
            <CR:CrystalReportViewer ID="CrystalReportViewerOne" runat="server" AutoDataBind="true" 
                 ReportSourceID="CrystalReportSourceOne" HasCrystalLogo="False" 
                 HasToggleGroupTreeButton="False" EnableDatabaseLogonPrompt="False"
                 ToolbarStyle-BorderStyle="None" ToolPanelView="None" Width="100%"
                 DisplayStatusbar="False" PrintMode="ActiveX" 
                 HasToggleParameterPanelButton="False" EnableDrillDown="False" />
            <br />
        </div>

        <asp:Label ID="lblMessage" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="Large" ForeColor="#FF3300"></asp:Label>

    </form>
</body>
</html>
