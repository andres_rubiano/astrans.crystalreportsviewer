﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;

namespace Simplexity.CrystalReportViewer
{
    public partial class ReportViewer : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        private void Page_Init(object sender, EventArgs e)
        {
            try
            {
                //var assemblies =
                //    from file in Directory.GetFiles(AppDomain.CurrentDomain.BaseDirectory + "bin")
                //    where Path.GetExtension(file) == ".dll"
                //    select Assembly.LoadFrom(file);

                //using (System.IO.StreamWriter file =
                //    new System.IO.StreamWriter(AppDomain.CurrentDomain.BaseDirectory + @"libdll.txt", true))
                //{
                //    foreach (var item in assemblies)
                //    {
                //        //lblMessage.Text += $"\n{item.FullName}|{item.ImageRuntimeVersion}|{item.Location}|{item.CodeBase}|{item.EscapedCodeBase}";
                //        file.WriteLine($"{item.FullName}|{item.ImageRuntimeVersion}|{item.Location}|{item.CodeBase}|{item.EscapedCodeBase}");
                //    }
                //}



                // nombre del reporte
                string reportName = Request.QueryString["ReportName"];
                // Id del registro de la tabla principal
                string reportParamId1 = Request.QueryString["strID1"];
                //si es borrador = 1 o original = 0
                string reportAction = Request.QueryString["Action"];
                // Copias del reporte
                string reportCopies = Request.QueryString["strRPCID"];

                var message = new System.Text.StringBuilder();
                if (string.IsNullOrEmpty(reportName))
                {
                    message.AppendLine("ReportName,");
                }
                if (string.IsNullOrEmpty(reportParamId1))
                {
                    message.AppendLine("strID1,");
                }
                if (string.IsNullOrEmpty(reportAction))
                {
                    message.AppendLine("Action,");
                }
                if (string.IsNullOrEmpty(reportCopies))
                {
                    message.AppendLine("strRPCID,");
                }

                if (message.Length > 0)
                {
                    lblMessage.Text = message.ToString().Remove(message.ToString().LastIndexOf(','));
                    return;
                }

                reportParamId1 = reportParamId1.Replace("|", ",");
                reportCopies = reportCopies.Replace("|", ",");

                reportName = ConfigurationManager.AppSettings["crystalReportPath"] + reportName;

                CrystalReportSourceOne.Report.FileName = reportName;
                var parametersReport = CrystalReportViewerOne.ParameterFieldInfo;
                CrystalReportSourceOne.ReportDocument.Load(reportName);

                //Conexion a la base de datos
                var crConnection = new CrystalDecisions.Shared.ConnectionInfo
                {
                    UserID = ConfigurationManager.AppSettings["sqlUser"],
                    ServerName = ConfigurationManager.AppSettings["sqlInstance"],
                    DatabaseName = ConfigurationManager.AppSettings["sqlDatabase"],
                    Password = ConfigurationManager.AppSettings["sqlPassword"]
                };

                AssignConnectionInfo(CrystalReportSourceOne.ReportDocument, crConnection);

                //Asignacion de parametros

                foreach (var parameter in parametersReport)
                {
                    var parameterField = ((ParameterField)parameter);

                    switch (parameterField.ParameterFieldName)
                    {
                        case "ID1":
                            CrystalReportSourceOne.ReportDocument.SetParameterValue("ID1", reportParamId1);
                            break;
                        case "Action":
                            CrystalReportSourceOne.ReportDocument.SetParameterValue("Action", reportAction);
                            break;
                        case "RPCID":
                            CrystalReportSourceOne.ReportDocument.SetParameterValue("RPCID", reportCopies);
                            break;
                    }
                }

                CrystalReportViewerOne.ReportSource = CrystalReportSourceOne.ReportDocument;
                CrystalReportViewerOne.DataBind();

            }
            catch (Exception ex)
            {
                using (StreamWriter file =
                    new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + @"libdll.txt", true))
                {
                    file.WriteLine(ex.Message);
                    file.WriteLine(ex.InnerException == null ? string.Empty : ex.InnerException.Message);
                    file.WriteLine(ex.InnerException.InnerException == null ? string.Empty : ex.InnerException.InnerException.Message);
                }

            }
        }

        private void AssignConnectionInfo(ReportDocument document, ConnectionInfo crConnection)
        {
            foreach (CrystalDecisions.CrystalReports.Engine.Table table in document.Database.Tables)
            {
                TableLogOnInfo logOnInfo = table.LogOnInfo;
                if (logOnInfo != null)
                {
                    table.LogOnInfo.TableName = table.Name;
                    table.LogOnInfo.ConnectionInfo.UserID = crConnection.UserID;
                    table.LogOnInfo.ConnectionInfo.Password = crConnection.Password;
                    table.LogOnInfo.ConnectionInfo.DatabaseName = crConnection.DatabaseName;
                    table.LogOnInfo.ConnectionInfo.ServerName = crConnection.ServerName;
                    table.ApplyLogOnInfo(table.LogOnInfo);

                    CrystalReportViewerOne.LogOnInfo.Add(table.LogOnInfo);
                }
            }
        }

    }
}